package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;
import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
      //Any method annotated with "@Before" will be executed before each test,
      //allowing the tester to set up some shared resources.
      @Before public void setUp() throws Exception {
        System.out.println("------------------------------SETTING UP-----------------------------");
      }

      //Any method annotated with "@After" will be executed after each test,
      //allowing the tester to release any shared resources used in the setup.
      @After public void tearDown() throws Exception {
        System.out.println("------------------------------TEARDOWN-------------------------------\n");
      }

      // test case for EC2-1
      @Test(expected = InvalidInstructionException.class)
      public void testEC2_1() throws Throwable{
        System.out.println("Running: test case for EC2-1");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-1.s");
        int expected = m.execute(lines);
      }

      // test case for EC2-2
      @Test
      public void testEC2_2() {
        System.out.println("Running: test case for EC2-2");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-2.s");
        int expected = m.execute(lines);
        assertEquals(expected,0);
      }

      // test case for EC2-3
      @Test(expected = InvalidInstructionException.class)
      public void testEC2_3() throws Throwable{
        System.out.println("Running: test case for EC2-3");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-3.s");
        int expected = m.execute(lines);
      }

      // test case for EC2-4
      @Test(expected = InvalidInstructionException.class)
      public void testEC2_4() throws Throwable{
        System.out.println("Running: test case for EC2-4");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-4.s");
        int expected = m.execute(lines);
      }

      // test case for EC2-5
      @Test
      public void testEC2_5(){
        System.out.println("Running: test case for EC2-5");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-5.s");
        int expected = m.execute(lines);
        assertEquals(expected,1);

      }

      // test case for EC2-6
      @Test
      public void testEC2_6(){
        System.out.println("Running: test case for EC2-6");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-6.s");
        int expected = m.execute(lines);
        assertEquals(expected,2);
      }

      // test case for EC2-7
      @Test(expected = InvalidInstructionException.class)
      public void testEC2_7() throws Throwable{
        System.out.println("Running: test case for EC2-7");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-7.s");
        int expected = m.execute(lines);
      }

      // test case for EC2-8
      @Test
      public void testEC2_8(){
        System.out.println("Running: test case for EC2-8");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-8.s");
        int expected = m.execute(lines);
        assertEquals(expected,-1);
      }

      // test case for EC2-9
      @Test
      public void testEC2_9(){
        System.out.println("Running: test case for EC2-9");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-9.s");
        int expected = m.execute(lines);
        assertEquals(expected,6);
      }

      // test case for EC2-10
      @Test
      public void testEC2_10(){
        System.out.println("Running: test case for EC2-10");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-10.s");
        int expected = m.execute(lines);
        assertEquals(expected,5);
      }

      // test case for EC2-11
      @Test
      public void testEC2_11(){
        System.out.println("Running: test case for EC2-11");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-11.s");
        int expected = m.execute(lines);
        assertEquals(expected,5);
      }

      // test case for EC2-12
      @Test(expected = InvalidInstructionException.class)
      public void testEC2_12() throws Throwable{
        System.out.println("Running: test case for EC2-12");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-12.s");
        int expected = m.execute(lines);
      }

      // test case for EC2-13
      @Test
      public void testEC2_13(){
        System.out.println("Running: test case for EC2-13");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-13.s");
        int expected = m.execute(lines);
        assertEquals(expected,-65535);
      }

    // test case for EC2-14
    @Test(expected = InvalidInstructionException.class)
        public void testEC2_14() throws Throwable{
        System.out.println("Running: test case for EC2-14");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-14.s");
        int expected = m.execute(lines);
    }

    // test case for EC2-15
    @Test public void testEC2_15(){
        System.out.println("Running: test case for EC2-15");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-15.s");
        int expected = m.execute(lines);
        assertEquals(expected,100);
    }

      // test case for EC2-16
    @Test public void testEC2_16() {
        System.out.println("Running: test case for EC2-16");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-16.s");
        int expected = m.execute(lines);
        assertEquals(expected,0);
    }

    // test case for EC2-17
    @Test public void testEC2_17(){
        System.out.println("Running: test case for EC2-17");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-17.s");
        int expected = m.execute(lines);
        assertEquals(expected,100);
    }

    //test case for EC2-18
    @Test public void testEC2_18(){
        System.out.println("Running: test case for EC2-18");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-18.s");
        int expected = m.execute(lines);
        assertEquals(expected,0);
    }

    // test case for EC2-19
    @Test public void testEC2_19(){
        System.out.println("Running: test case for EC2-19");
        Long t1 = System.currentTimeMillis();
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-19.s");
        System.out.println("this is an infinite loop");
//        int expected = m.execute(lines);
//        assertEquals(expected,0);
    }

    //test case for EC2-20
    @Test public void testEC2_20(){
        System.out.println("Running: test case for EC2-20");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-20.s");
        int expected = m.execute(lines);
        assertEquals(expected,10);
    }

    //test case for EC2-21
    @Test public void testEC2_21(){
        System.out.println("Running: test case for EC2-21");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-21.s");
        int expected = m.execute(lines);
        assertEquals(expected,1);
    }

    //test case for EC2-22
    @Test(expected = NoReturnValueException.class)
    public void testEC2_22() throws Throwable{
        System.out.println("Running: test case for EC2-22");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-22.s");
        int expected = m.execute(lines);
    }


    //test case for EC2-23
    @Test public void testEC2_23(){
        System.out.println("Running: test case for EC2-23");
        System.out.println("This is an infinite loop");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-23.s");
//        int expected = m.execute(lines);
//        assertEquals(expected,0);
    }

    //test case for EC2-24
    @Test public void testEC2_24(){
        System.out.println("Running: test case for EC2-24");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-24.s");
        int expected = m.execute(lines);
        assertEquals(expected,0);
    }

    //test case for EC2-25
    @Test public void testEC2_25(){
        System.out.println("Running: test case for EC2-25");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/EC2-25.s");
        int expected = m.execute(lines);
        assertEquals(expected,5);
    }

    //Test test opens a file and executes the machine
//    @Test public void aFileOpenTest()
//    {
//        System.out.println("Running: test open file");
//        final List<String> lines = readInstructions("examples/array.s");
//        Machine m = new Machine();
//        assertEquals(m.execute(lines), 45);
//    }

    //Read in a file containing a program and convert into a list of
    //string instructions
    private List<String> readInstructions(String file)
    {
        Charset charset = Charset.forName("UTF-8");
        List<String> lines = null;
        try {
          lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
        }
        catch (Exception e){
          System.err.println("Invalid input file! (stacktrace follows)");
          e.printStackTrace(System.err);
          System.exit(1);
        }
        return lines;
    }
}
