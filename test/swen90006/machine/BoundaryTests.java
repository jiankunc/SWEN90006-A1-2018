package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;

import static org.junit.Assert.*;

public class BoundaryTests {
    //Any method annotated with "@Before" will be executed before each test,
    //allowing the tester to set up some shared resources.
    @Before public void setUp() {
        System.out.println("------------------------------SETTING UP-----------------------------");
    }

    //Any method annotated with "@After" will be executed after each test,
    //allowing the tester to release any shared resources used in the setup.
    @After public void tearDown() {
        System.out.println("------------------------------TEARDOWN-------------------------------\n");
    }

    // test case for B1_1
    @Test public void testB1_1() {
        System.out.println("Running: test case for B1_1");
        Machine m = new Machine();
        List<String> lines = new ArrayList<String>();
        lines.add("RET R1");
        int expected = m.execute(lines);
        assertEquals(expected, 0);
    }

    // test case for B1_2
    @Test public void testB1_2() {
        System.out.println("Running: test case for B1_2");
        Machine m = new Machine();
        List<String> lines = new ArrayList<String>();
        lines.add("RET R31");
        int expected = m.execute(lines);
        assertEquals(expected, 0);
    }

    // test case for B1_3
    @Test(expected = InvalidInstructionException.class)
    public void testB1_3() throws Throwable {
        System.out.println("Running: test case for B1_3");
        Machine m = new Machine();
        List<String> lines = new ArrayList<String>();
        lines.add("RET R-1");
        int expected = m.execute(lines);
    }

    // test case for B1_4
    @Test(expected = InvalidInstructionException.class)
    public void testB1_4() throws Throwable {
        System.out.println("Running: test case for B1_4");
        Machine m = new Machine();
        List<String> lines = new ArrayList<String>();
        lines.add("RET R32");
        int expected = m.execute(lines);
    }


    //test case for B2_1
    @Test public void testB2_1(){
        System.out.println("Running: test case for B2_1");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B2_1.s");
        int expected = m.execute(lines);
        assertEquals(expected,5);
    }


    // test case for B2_2
    @Test(expected = InvalidInstructionException.class)
    public void testB2_2() throws Throwable {
        System.out.println("Running: test case for B2_2");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B2_2.s");
        int expected = m.execute(lines);

    }

    // test case for B2_3
    @Test(expected = InvalidInstructionException.class)
    public void testB2_3() throws Throwable {
        System.out.println("Running: test case for B2_3");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B2_3.s");
        int expected = m.execute(lines);

    }


    //test case for B3_1
    @Test public void testB3_1(){
        System.out.println("Running: test case for B3_1");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B3_1.s");
        int expected = m.execute(lines);
        assertEquals(expected,2);
    }


    // test case for B3_2
    @Test(expected = InvalidInstructionException.class)
    public void testB3_2() throws Throwable {
        System.out.println("Running: test case for B3_2");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B3_2.s");
        int expected = m.execute(lines);

    }

    // test case for B3_3
    @Test(expected = InvalidInstructionException.class)
    public void testB3_3() throws Throwable {
        System.out.println("Running: test case for B3_3");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B3_3.s");
        int expected = m.execute(lines);
    }

    //test case for B4_1
    @Test public void testB4_1(){
        System.out.println("Running: test case for B4_1");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B4_1.s");
        int expected = m.execute(lines);
        assertEquals(expected,0);
    }


    // test case for B4_2
    @Test(expected = InvalidInstructionException.class)
    public void testB4_2() throws Throwable {
        System.out.println("Running: test case for B4_2");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B4_2.s");
        int expected = m.execute(lines);

    }

    // test case for B4_3
    @Test(expected = InvalidInstructionException.class)
    public void testB4_3() throws Throwable {
        System.out.println("Running: test case for B4_3");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B4_3.s");
        int expected = m.execute(lines);
    }

    //test case for B5_1
    @Test public void testB5_1(){
        System.out.println("Running: test case for B5_1");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B5_1.s");
        int expected = m.execute(lines);
        assertEquals(expected,2);
    }

    // test case for B5_2
    @Test(expected = InvalidInstructionException.class)
    public void testB5_2() throws Throwable {
        System.out.println("Running: test case for B5_2");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B5_2.s");
        int expected = m.execute(lines);

    }

    // test case for B5_3
    @Test(expected = InvalidInstructionException.class)
    public void testB5_3() throws Throwable {
        System.out.println("Running: test case for B5_3");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B5_3.s");
        int expected = m.execute(lines);
    }

    //test case for B6_1
    @Test public void testB6_1(){
        System.out.println("Running: test case for B6_1");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B6_1.s");
        int expected = m.execute(lines);
        assertEquals(expected,2);
    }

    //test case for B6_2
    @Test public void testB6_2(){
        System.out.println("Running: test case for B6_2");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B6_2.s");
        int expected = m.execute(lines);
        assertEquals(expected,2);
    }

    // test case for B6_3
    @Test(expected = InvalidInstructionException.class)
    public void testB6_3() throws Throwable {
        System.out.println("Running: test case for B6_3");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B6_3.s");
        int expected = m.execute(lines);

    }

    // test case for B6_4
    @Test(expected = InvalidInstructionException.class)
    public void testB6_4() throws Throwable {
        System.out.println("Running: test case for B6_4");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B6_4.s");
        int expected = m.execute(lines);
    }


    //test case for B7_1
    @Test public void testB7_1(){
        System.out.println("Running: test case for B7_1");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B7_1.s");
        int expected = m.execute(lines);
        assertEquals(expected,-65535);
    }

    //test case for B7_2
    @Test public void testB7_2(){
        System.out.println("Running: test case for B7_2");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B7_2.s");
        int expected = m.execute(lines);
        assertEquals(expected,65535);
    }

    // test case for B7_3
    @Test(expected = InvalidInstructionException.class)
    public void testB7_3() throws Throwable {
        System.out.println("Running: test case for B7_3");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B7_3.s");
        int expected = m.execute(lines);

    }

    // test case for B7_4
    @Test(expected = InvalidInstructionException.class)
    public void testB7_4() throws Throwable {
        System.out.println("Running: test case for B7_4");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B7_4.s");
        int expected = m.execute(lines);
    }

    //test case for B8_1
    @Test public void testB8_1(){
        System.out.println("Running: test case for B8_1");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B8_1.s");
        int expected = m.execute(lines);
        assertEquals(expected,0);
    }

    // test case for B8_2
    @Test(expected = InvalidInstructionException.class)
    public void testB8_2() throws Throwable {
        System.out.println("Running: test case for B8_2");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B8_2.s");
        int expected = m.execute(lines);
    }

    // test case for B8_3
    @Test(expected = InvalidInstructionException.class)
    public void testB8_3() throws Throwable {
        System.out.println("Running: test case for B8_3");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B8_3.s");
        int expected = m.execute(lines);
    }

    //test case for B8_4
    @Test public void testB8_4(){
        System.out.println("Running: test case for B8_4");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B8_4.s");
        int expected = m.execute(lines);
        assertEquals(expected,0);
    }

    // test case for B8_5
    @Test(expected = InvalidInstructionException.class)
    public void testB8_5() throws Throwable {
        System.out.println("Running: test case for B8_5");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B8_5.s");
        int expected = m.execute(lines);
    }

    // test case for B8_6
    @Test(expected = InvalidInstructionException.class)
    public void testB8_6() throws Throwable {
        System.out.println("Running: test case for B8_6");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B8_6.s");
        int expected = m.execute(lines);
    }


    //test case for B9_1
    @Test public void testB9_1(){
        System.out.println("Running: test case for B9_1");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B9_1.s");
        int expected = m.execute(lines);
        assertEquals(expected,0);
    }

    //test case for B9_2
    @Test public void testB9_2(){
        System.out.println("Running: test case for B9_2");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B9_2.s");
        int expected = m.execute(lines);
        assertEquals(expected,0);
    }

    // test case for B9_3
    @Test(expected = InvalidInstructionException.class)
    public void testB9_3() throws Throwable {
        System.out.println("Running: test case for B9_3");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B9_3.s");
        int expected = m.execute(lines);
    }

    // test case for B9_4
    @Test(expected = InvalidInstructionException.class)
    public void testB9_4() throws Throwable {
        System.out.println("Running: test case for B9_4");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B9_4.s");
        int expected = m.execute(lines);
    }

    // test case for B9_5
    @Test(expected = InvalidInstructionException.class)
    public void testB9_5() throws Throwable {
        System.out.println("Running: test case for B9_5");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B9_5.s");
        int expected = m.execute(lines);
    }

    // test case for B9_6
    @Test(expected = InvalidInstructionException.class)
    public void testB9_6() throws Throwable {
        System.out.println("Running: test case for B9_6");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B9_6.s");
        int expected = m.execute(lines);
    }

    //test case for B10_1
    @Test public void testB10_1(){
        System.out.println("Running: test case for B10_1");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B10_1.s");
        int expected = m.execute(lines);
        assertEquals(expected,2);
    }

    //test case for B10_2
    @Test public void testB10_2(){
        System.out.println("Running: test case for B10_2");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B10_2.s");
        int expected = m.execute(lines);
        assertEquals(expected,2);
    }

    // test case for B10_3
    @Test
    public void testB10_3(){
        System.out.println("Running: test case for B10_3");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B10_3.s");
        int expected = m.execute(lines);
        assertEquals(expected,0);
    }

    // test case for B10_4
    @Test
    public void testB10_4(){
        System.out.println("Running: test case for B10_4");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B10_4.s");
        int expected = m.execute(lines);
        assertEquals(expected,0);
    }

    // test case for B10_5
    @Test
    public void testB10_5(){
        System.out.println("Running: test case for B10_5");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B10_5.s");
        int expected = m.execute(lines);
        assertEquals(expected,0);
    }

    // test case for B10_6
    @Test
    public void testB10_6(){
        System.out.println("Running: test case for B10_6");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B10_6.s");
        int expected = m.execute(lines);
        assertEquals(expected,0);
    }

    //test case for B11_1
    @Test public void testB11_1(){
        System.out.println("Running: test case for B11_1");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B11_1.s");
        int expected = m.execute(lines);
        assertEquals(expected,2);
    }

    //test case for B11_2
    @Test public void testB11_2(){
        System.out.println("Running: test case for B11_2");
        System.out.println("this is an infinite loop");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B11_2.s");
//        int expected = m.execute(lines);
//        assertEquals(expected,2);
    }

    // test case for B11_3
    @Test
    public void testB11_3(){
        System.out.println("Running: test case for B11_3");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B11_3.s");
        int expected = m.execute(lines);
        assertEquals(expected,2);
    }

    // test case for B11_4
    @Test
    public void testB11_4(){
        System.out.println("Running: test case for B11_4");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B11_4.s");
        int expected = m.execute(lines);
        assertEquals(expected,4);
    }

    // test case for B12_1
    @Test
    public void testB12_1(){
        System.out.println("Running: test case for B12_1");
        System.out.println("this is an infinite loop");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B12_1.s");
//        int expected = m.execute(lines);
//        assertEquals(expected,2);
    }

    // test case for B12_2
    @Test
    public void testB12_2(){
        System.out.println("Running: test case for B12_2");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B12_2.s");
        int expected = m.execute(lines);
        assertEquals(expected,2);
    }

    // test case for B12_3
    @Test(expected = NoReturnValueException.class)
    public void testB12_3() throws Throwable {
        System.out.println("Running: test case for B12_3");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B12_3.s");
        int expected = m.execute(lines);
    }

    // test case for B12_4
    @Test(expected = NoReturnValueException.class)
    public void testB12_4() throws Throwable {
        System.out.println("Running: test case for B12_4");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B12_4.s");
        int expected = m.execute(lines);
    }

    // test case for B13_1
    @Test
    public void testB13_1(){
        System.out.println("Running: test case for B13_1");
        System.out.println("this is an infinite loop");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B13_1.s");
        int expected = m.execute(lines);
        assertEquals(expected,0);
    }

    // test case for B13_2
    @Test
    public void testB13_2(){
        System.out.println("Running: test case for B13_2");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B13_2.s");
        int expected = m.execute(lines);
        assertEquals(expected,-1);
    }

    // test case for B13_3
    @Test public void testB13_3(){
        System.out.println("Running: test case for B13_3");
        Machine m = new Machine();
        final List<String> lines = readInstructions("./examples/B13_3.s");
        int expected = m.execute(lines);
        assertEquals(expected,2);
    }


//  @Test public void anotherTest()
//  {
//    List<String> list = new ArrayList<String>();
//    list.add("a");
//    list.add("b");
//
//    //the assertTrue method is used to check whether something holds.
//    assertTrue(list.contains("a"));
//  }

    //Test test opens a file and executes the machine
//  @Test public void aFileOpenTest()
//  {
//    final List<String> lines = readInstructions("examples/array.s");
//    Machine m = new Machine();
//    assertEquals(m.execute(lines), 45);
//  }

    //Read in a file containing a program and convert into a list of
    //string instructions
    private List<String> readInstructions(String file) {
        Charset charset = Charset.forName("UTF-8");
        List<String> lines = null;
        try {
            lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
        } catch (Exception e) {
            System.err.println("Invalid input file! (stacktrace follows)");
            e.printStackTrace(System.err);
            System.exit(1);
        }
        return lines;
    }
}
